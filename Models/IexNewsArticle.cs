using System;
using Newtonsoft.Json;

namespace stock_up_web.Models
{
    public class IexNewsArticle
    {

        // [
        //     {
        //         "datetime": 1545215400000,
        //         "headline": "Voice Search Technology Creates A New Paradigm For Marketers",
        //         "source": "Benzinga",
        //         "url": "https://cloud.iexapis.com/stable/news/article/8348646549980454",
        //         "summary": "<p>Voice search is likely to grow by leap and bounds, with technological advancements leading to better adoption and fueling the growth cycle, according to Lindsay Boyajian, <a href=\"http://loupventures.com/how-the-future-of-voice-search-affects-marketers-today/\">a guest contributor at Loup Ventu...",
        //         "related": "AAPL,AMZN,GOOG,GOOGL,MSFT",
        //         "image": "https://cloud.iexapis.com/stable/news/image/7594023985414148",
        //         "lang": "en",
        //         "hasPaywall": true
        //     }
        // ]

        [JsonProperty("datetime")]
        public Int64 datetime { get; set; }

        [JsonProperty("headline")]
        public string headline { get; set; }

        [JsonProperty("source")]
        public string source { get; set; }

        [JsonProperty("url")]
        public string url { get; set; }

        [JsonProperty("summary")]
        public string summary { get; set; }

        [JsonProperty("related")]
        public string related { get; set; }

        [JsonProperty("image")]
        public string image { get; set; }

        [JsonProperty("lang")]
        public string lang { get; set; }
        
        [JsonProperty("hasPaywall")]
        public bool? hasPaywall { get; set; }
        
    }
}