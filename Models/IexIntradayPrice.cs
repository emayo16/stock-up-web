using System;
using Newtonsoft.Json;

namespace stock_up_web.Models
{
    public class IexIntradayPrice {

        #region Intraday Price JSON example
        // {
        //         "date": "2017-12-15",
        //         "minute": "09:30",
        //         "label": "09:30 AM",
        //         "marktOpen": 143.98,
        //         "marketClose": 143.775,
        //         "marktHigh": 143.98,
        //         "marketLow": 143.775,
        //         "marketAverage": 143.889,
        //         "marketVolume": 3070,
        //         "marketNotional": 441740.275,
        //         "marketNumberOfTrades": 20,
        //         "marketChangeOverTime": -0.004,
        //         "high": 143.98,
        //         "low": 143.775,
        //         "open": 143.98,
        //         "close": 143.775,
        //         "average": 143.889,
        //         "volume": 3070,
        //         "notional": 441740.275,
        //         "numberOfTrades": 20,
        //         "changeOverTime": -0.0039,
        //     } 
        #endregion Intraday Price JSON example

        [JsonProperty("date")]
        public string date { get; set; }

        [JsonProperty("minute")]
        public string minute { get; set; }

        [JsonProperty("label")]
        public string label { get; set; }

        [JsonProperty("marktOpen")]
        public decimal? marktOpen { get; set; }

        [JsonProperty("marketClose")]
        public decimal? marketClose { get; set; }

        [JsonProperty("marktHigh")]
        public decimal? marktHigh { get; set; }

        [JsonProperty("marketLow")]
        public decimal? marketLow { get; set; }

        [JsonProperty("marketAverage")]
        public decimal? marketAverage { get; set; }

        [JsonProperty("marketVolume")]
        public decimal? marketVolume { get; set; }

        [JsonProperty("marketNotional")]
        public decimal? marketNotional { get; set; }

        [JsonProperty("marketNumberOfTrades")]
        public Int64? marketNumberOfTrades { get; set; }

        [JsonProperty("marketChangeOverTime")]
        public decimal? marketChangeOverTime { get; set; }

        [JsonProperty("high")]
        public decimal? high { get; set; }

        [JsonProperty("low")]
        public decimal? low { get; set; }

        [JsonProperty("open")]
        public decimal? open { get; set; }

        [JsonProperty("close")]
        public decimal? close { get; set; }

        [JsonProperty("average")]
        public decimal? average { get; set; }

        [JsonProperty("volume")]
        public decimal? volume { get; set; }

        [JsonProperty("notional")]
        public decimal? notional { get; set; }

        [JsonProperty("numberOfTrades")]
        public Int64? numberOfTrades { get; set; }

        [JsonProperty("changeOverTime")]
        public decimal? changeOverTime { get; set; }

    }
}