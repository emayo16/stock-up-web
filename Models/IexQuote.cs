using System;
using Newtonsoft.Json;

namespace stock_up_web.Models
{
    // Example IexQuote
    // {
    //     "symbol": "AAPL",
    //     "companyName": "Apple Inc.",
    //     "calculationPrice": "tops",
    //     "open": 154,
    //     "openTime": 1506605400394,
    //     "close": 153.28,
    //     "closeTime": 1506605400394,
    //     "high": 154.80,
    //     "low": 153.25,
    //     "latestPrice": 158.73,
    //     "latestSource": "Previous close",
    //     "latestTime": "September 19, 2017",
    //     "latestUpdate": 1505779200000,
    //     "latestVolume": 20567140,
    //     "iexRealtimePrice": 158.71,
    //     "iexRealtimeSize": 100,
    //     "iexLastUpdated": 1505851198059,
    //     "delayedPrice": 158.71,
    //     "delayedPriceTime": 1505854782437,
    //     "extendedPrice": 159.21,
    //     "extendedChange": -1.68,
    //     "extendedChangePercent": -0.0125,
    //     "extendedPriceTime": 1527082200361,
    //     "previousClose": 158.73,
    //     "change": -1.67,
    //     "changePercent": -0.01158,
    //     "iexMarketPercent": 0.00948,
    //     "iexVolume": 82451,
    //     "avgTotalVolume": 29623234,
    //     "iexBidPrice": 153.01,
    //     "iexBidSize": 100,
    //     "iexAskPrice": 158.66,
    //     "iexAskSize": 100,
    //     "marketCap": 751627174400,
    //     "week52High": 159.65,
    //     "week52Low": 93.63,
    //     "ytdChange": 0.3665,
    // }

    public class IexQuote
    {
        [JsonProperty("symbol")]
        public string symbol { get; set; }

        [JsonProperty("companyName")]
        public string companyName { get; set; }

        [JsonProperty("calculationPrice")]
        public string calculationPrice { get; set; }

        [JsonProperty("open")]
        public decimal? open { get; set; }

        [JsonProperty("openTime")]
        public Int64? openTime { get; set; }

        [JsonProperty("close")]
        public decimal? close { get; set; }

        [JsonProperty("closeTime")]
        public Int64? closeTime { get; set; }

        [JsonProperty("high")]
        public decimal? high { get; set; }

        [JsonProperty("low")]
        public decimal? low { get; set; }

        [JsonProperty("latestPrice")]
        public decimal? latestPrice { get; set; }

        [JsonProperty("latestSource")]
        public string latestSource { get; set; }

        [JsonProperty("latestTime")]
        public string latestTime { get; set; }

        [JsonProperty("latestUpdate")]
        public Int64? latestUpdate { get; set; }

        [JsonProperty("latestVolume")]
        public Int64? latestVolume { get; set; }

        [JsonProperty("iexRealtimePrice")]
        public decimal? iexRealtimePrice { get; set; }

        [JsonProperty("iexRealtimeSize")]
        public Int64? iexRealtimeSize { get; set; }

        [JsonProperty("iexLastUpdated")]
        public Int64? iexLastUpdated { get; set; }

        [JsonProperty("delayedPrice")]
        public decimal? delayedPrice { get; set; }

        [JsonProperty("delayedPriceTime")]
        public Int64? delayedPriceTime { get; set; }

        [JsonProperty("extendedPrice")]
        public decimal? extendedPrice { get; set; }

        [JsonProperty("extendedChange")]
        public decimal? extendedChange { get; set; }

        [JsonProperty("extendedChangePercent")]
        public decimal? extendedChangePercent { get; set; }

        [JsonProperty("extendedPriceTime")]
        public Int64? extendedPriceTime { get; set; }

        [JsonProperty("previousClose")]
        public decimal? previousClose { get; set; }

        [JsonProperty("change")]
        public decimal? change { get; set; }

        [JsonProperty("changePercent")]
        public decimal? changePercent { get; set; }

        [JsonProperty("iexMarketPercent")]
        public decimal? iexMarketPercent { get; set; }

        [JsonProperty("iexVolume")]
        public Int64? iexVolume { get; set; }

        [JsonProperty("avgTotalVolume")]
        public Int64? avgTotalVolume { get; set; }

        [JsonProperty("iexBidPrice")]
        public decimal? iexBidPrice { get; set; }

        [JsonProperty("iexBidSize")]
        public Int64? iexBidSize { get; set; }

        [JsonProperty("iexAskPrice")]
        public decimal? iexAskPrice { get; set; }

        [JsonProperty("iexAskSize")]
        public Int64? iexAskSize { get; set; }

        [JsonProperty("marketCap")]
        public decimal? marketCap { get; set; }

        [JsonProperty("week52High")]
        public decimal? week52High { get; set; }

        [JsonProperty("week52Low")]
        public decimal? week52Low { get; set; }

        [JsonProperty("ytdChange")]
        public decimal? ytdChange { get; set; }
    }
}