using System;
using Newtonsoft.Json;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace stock_up_web.Models
{
    public class IexSymbol {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("symbol")]
        [JsonProperty("symbol")]
        public string symbol { get; set; }

        [BsonElement("exchange")]
        [JsonProperty("exchange")]
        public string exchange { get; set; }

        [BsonElement("name")]
        [JsonProperty("name")]
        public string name { get; set; }

        [BsonElement("date")]
        [JsonProperty("date")]
        public DateTime date { get; set; }

        [BsonElement("type")]
        [JsonProperty("type")]
        public string type { get; set; }

        [BsonElement("iexId")]
        [JsonProperty("iexId")]
        public string iexId { get; set; }

        [BsonElement("region")]
        [JsonProperty("region")]
        public string region { get; set; }

        [BsonElement("currency")]
        [JsonProperty("currency")]
        public string currency { get; set; }
        
        [BsonElement("isEnabled")]
        [JsonProperty("isEnabled")]
        public bool isEnabled { get; set; }
        
    }
}