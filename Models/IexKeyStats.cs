using System;
using Newtonsoft.Json;

namespace stock_up_web.Models
{
    public class IexKeyStats {
        
        #region Key Stats JSON example
        // {
        //     "companyName": "Apple Inc.",
        //     "marketcap": 760334287200,
        //     "week52high": 156.65,
        //     "week52low": 93.63,
        //     "week52change": 58.801903,
        //     "sharesOutstanding": 5213840000,
        //     "float": 5203997571,
        //     "symbol": "AAPL",
        //     "avg10Volume": 2774000,
        //     "avg30Volume": 12774000,
        //     "day200MovingAvg": 140.60541,
        //     "day50MovingAvg": 156.49678,
        //     "employees": 120000,
        //     "ttmEPS": 16.5,
        //     "ttmDividendRate": 2.25,
        //     "dividendYield": .021,
        //     "nextDividendDate": '2019-03-01',
        //     "exDividendDate": '2019-02-08',
        //     "nextEarningsDate": '2019-01-01',
        //     "peRatio": 14,
        //     "beta": 1.25,
        //     "maxChangePercent": 153.021,
        //     "year5ChangePercent": 0.5902546932200027,
        //     "year2ChangePercent": 0.3777449874142869,
        //     "year1ChangePercent": 0.39751716851558366,
        //     "ytdChangePercent": 0.36659492036160124,
        //     "month6ChangePercent": 0.12208398133748043,
        //     "month3ChangePercent": 0.08466584665846649,
        //     "month1ChangePercent": 0.009668596145283263,
        //     "day30ChangePercent": -0.002762605699968781,
        //     "day5ChangePercent": -0.005762605699968781
        //     }
        #endregion Key Stats JSON example

        [JsonProperty("companyName")]
        public string companyName { get; set; }

        [JsonProperty("marketCap")]
        public decimal? marketcap { get; set; }

        [JsonProperty("week52high")]
        public decimal? week52high { get; set; }

        [JsonProperty("week52low")]
        public decimal? week52low { get; set; }

        [JsonProperty("week52change")]
        public decimal? week52change { get; set; }

        [JsonProperty("sharesOutstanding")]
        public decimal? sharesOutstanding { get; set; }

        [JsonProperty("float")]
        public decimal? iexfloat { get; set; }

        [JsonProperty("symbol")]
        public string symbol { get; set; }

        [JsonProperty("avg10volume")]
        public decimal? avg10Volume { get; set; }

        [JsonProperty("avg30volume")]
        public decimal? avg30Volume { get; set; }

        [JsonProperty("day200MovingAvg")]
        public decimal? day200MovingAvg { get; set; }

        [JsonProperty("day50MovingAvg")]
        public decimal? day50MovingAvg { get; set; }

        [JsonProperty("employees")]
        public decimal? employees { get; set; }

        [JsonProperty("ttmEPS")]
        public decimal? ttmEPS { get; set; }

        [JsonProperty("ttmDividendRate")]
        public decimal? ttmDividendRate { get; set; }

        [JsonProperty("dividendYield")]
        public decimal? dividendYield { get; set; }

        [JsonProperty("nextDividendDate")]
        public DateTime? nextDividendDate { get; set; }

        [JsonProperty("exDividendDate")]
        public DateTime? exDividendDate { get; set; }

        [JsonProperty("nextEarningsDate")]
        public DateTime? nextEarningsDate { get; set; }

        [JsonProperty("peRatio")]
        public decimal? peRatio { get; set; }

        [JsonProperty("beta")]
        public decimal? beta { get; set; }

        [JsonProperty("maxChangePercent")]
        public decimal? maxChangePercent { get; set; }

        [JsonProperty("year5ChangePercent")]
        public decimal? year5ChangePercent { get; set; }

        [JsonProperty("year2ChangePercent")]
        public decimal? year2ChangePercent { get; set; }

        [JsonProperty("year1ChangePercent")]
        public decimal? year1ChangePercent { get; set; }

        [JsonProperty("ytdChangePercent")]
        public decimal? ytdChangePercent { get; set; }

        [JsonProperty("month6ChangePercent")]
        public decimal? month6ChangePercent { get; set; }

        [JsonProperty("month3ChangePercent")]
        public decimal? month3ChangePercent { get; set; }

        [JsonProperty("month1ChangePercent")]
        public decimal? month1ChangePercent { get; set; }

        [JsonProperty("day30ChangePercent")]
        public decimal? day30ChangePercent { get; set; }

        [JsonProperty("day5ChangePercent")]
        public decimal? day5ChangePercent { get; set; }
        
    }
}