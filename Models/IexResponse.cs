using System.Net;
namespace stock_up_web.Models
{
    public class IexResponse
    {
        private WebResponse _response;
        private int _statusCode;
        public bool Ok { get; set; }
        public string Result { get; set;}
        public int StatusCode {
            get {
                return _statusCode;
            }
         }

        public IexResponse(WebResponse response) {
            _response = response;
            _statusCode = (int)((HttpWebResponse)response).StatusCode;
            Ok = StatusCode == 200;
        }
    }
}