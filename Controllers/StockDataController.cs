using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using stock_up_web.Models;
using stock_up_web.Services;

namespace stock_up_web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StockDataController : Controller
    {
        private readonly IexSymbolService _iexSymbolService;
        private readonly IConfiguration _configuration;
        private string IEX_API_KEY = Environment.GetEnvironmentVariable("IEX_API_KEY");
        private IConfigurationSection IEX_CONFIG;

        public StockDataController(IConfiguration configuration, IexSymbolService iexSymbolService)
        {
            _configuration = configuration;
            IEX_CONFIG = _configuration.GetSection("IEX");
            _iexSymbolService = iexSymbolService;
        }

        private async Task<string> ReadResponseStreamAsync(WebResponse response) {
            // Get the stream containing content returned by the server. 
            // The using block ensures the stream is automatically closed. 
            using (var dataStream = response.GetResponseStream())
            {
                // Open the stream using a StreamReader for easy access.  
                var reader = new StreamReader(dataStream);
                // Read the content Asynchronously.
                return await reader.ReadToEndAsync();
            }
        }

        private async Task<IexResponse> GetResponseAsync(string uri) {
            // Create a request for the URL.   
            var request = WebRequest.Create(uri);
            // Get the response.  
            var response = request.GetResponse();
            var iexResponse = new IexResponse(response);
            // Check status code.  
            if (iexResponse.Ok) {
                iexResponse.Result = await ReadResponseStreamAsync(response);
            }
            
            // Close the response.  
            response.Close();

            return iexResponse;
        }

        // GET /stock/market/list/{list-type}
        [HttpGet("[action]/{listType}")]
        public async Task<IEnumerable<IexQuote>> GetQuoteList(string listType) {
            var iexQuotes = new List<IexQuote>();
            if (!string.IsNullOrWhiteSpace(listType)) {
                string listUri = $"/stock/market/list/{listType}";
                string apiUri = IEX_CONFIG.GetValue<string>("API_URL");
                string uri = $"{apiUri}{listUri}?token={IEX_API_KEY}";
                var response = await GetResponseAsync(uri);
                if (response.Ok) {
                    iexQuotes = JsonConvert.DeserializeObject<List<IexQuote>>(response.Result);
                }
            }

            return iexQuotes;
        }

        // GET /stock/{symbol}/news
        [HttpGet("[action]/{symbol}/{count}")]
        public async Task<IEnumerable<IexNewsArticle>> GetNewsArticles(string symbol, int count) {
            var iexNewsArticles = new List<IexNewsArticle>();
            if (!string.IsNullOrWhiteSpace(symbol)) {
                string newsUri = $"/stock/{symbol}/news/last/{count}";
                string apiUri = IEX_CONFIG.GetValue<string>("API_URL");
                string uri = $"{apiUri}{newsUri}?token={IEX_API_KEY}";
                var response = await GetResponseAsync(uri);
                if (response.Ok) {
                    iexNewsArticles = JsonConvert.DeserializeObject<List<IexNewsArticle>>(response.Result);
                }
            }

            return iexNewsArticles;
        }

        // GET /stock/{symbol}/stats
        [HttpGet("[action]/{symbol}")]
        public async Task<IexKeyStats> GetKeyStats(string symbol) {
            var iexKeyStats = new IexKeyStats();
            if (!string.IsNullOrWhiteSpace(symbol)) {
                string keyStatsUri = $"/stock/{symbol}/stats";
                string apiUri = IEX_CONFIG.GetValue<string>("API_URL");
                string uri = $"{apiUri}{keyStatsUri}?token={IEX_API_KEY}";
                var response = await GetResponseAsync(uri);
                if (response.Ok) {
                    iexKeyStats = JsonConvert.DeserializeObject<IexKeyStats>(response.Result);
                }
            }

            return iexKeyStats;
        }

        [HttpGet("[action]")]
        public async Task<IEnumerable<IexSymbol>> GetSupportedSymbols() {
            return await _iexSymbolService.Get();
        }
        
        // GET /stock/{symbol}/intraday-prices
        [HttpGet("[action]/{symbol}")]
        public async Task<IEnumerable<IexIntradayPrice>> GetIntradayPrices(string symbol) {
            var iexIntradayPrices = new List<IexIntradayPrice>();
            if (!string.IsNullOrWhiteSpace(symbol)) {
                string intradayPricesUri = $"/stock/{symbol}/intraday-prices";
                string apiUri = IEX_CONFIG.GetValue<string>("API_URL");
                string uri = $"{apiUri}{intradayPricesUri}?token={IEX_API_KEY}";
                var response = await GetResponseAsync(uri);
                if (response.Ok) {
                    iexIntradayPrices = JsonConvert.DeserializeObject<List<IexIntradayPrice>>(response.Result);
                    for (int i = 0; i < iexIntradayPrices.Count; i++) {
                        iexIntradayPrices[i].date = $"{iexIntradayPrices[i].date}T{iexIntradayPrices[i].minute}";
                    }
                }
            }

            return iexIntradayPrices;
        }
    }
}
