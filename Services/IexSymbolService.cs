using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using stock_up_web.Models;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Core;
using Newtonsoft.Json;

namespace stock_up_web.Services
{
    public class IexSymbolService
    {
        private readonly IMongoCollection<IexSymbol> _iexSymbols;
        private readonly IConfiguration _configuration;
        private string IEX_API_KEY = Environment.GetEnvironmentVariable("IEX_API_KEY");
        private string MONGODB_URI = Environment.GetEnvironmentVariable("MONGODB_URI");
        private string MONGODB_NAME = Environment.GetEnvironmentVariable("MONGODB_NAME");
        private IConfigurationSection IEX_CONFIG;

        public IexSymbolService(IConfiguration configuration)
        {
            _configuration = configuration;
            IEX_CONFIG = _configuration.GetSection("IEX");
            var client = new MongoClient(MONGODB_URI);
            var database = client.GetDatabase(MONGODB_NAME);
            _iexSymbols = database.GetCollection<IexSymbol>("iex-symbol");
        }

        public async Task<IEnumerable<IexSymbol>> GetSymbolsFromJson() {
            string file = await File.ReadAllTextAsync(@"./LocalStorage/IexSymbols.json");
            return JsonConvert.DeserializeObject<IEnumerable<IexSymbol>>(file);
        }

        public async Task RefreshSupportedSymbols() {
            await this.RemoveAll();
            string apiUri = IEX_CONFIG.GetValue<string>("API_URL");
            string supportedSymbolsUri = IEX_CONFIG.GetValue<string>("SUPPORTED_SYMBOLS_URI");
            string uri = $"{apiUri}{supportedSymbolsUri}?token={IEX_API_KEY}";
            // Create a request for the URL.   
            WebRequest request = WebRequest.Create(uri);
            // Get the response.  
            WebResponse response = request.GetResponse();
            // Check status code.  
            if ((int)((HttpWebResponse)response).StatusCode == 200) {
                // Get the stream containing content returned by the server. 
                // The using block ensures the stream is automatically closed. 
                using (Stream dataStream = response.GetResponseStream())
                {
                    // Open the stream using a StreamReader for easy access.  
                    StreamReader reader = new StreamReader(dataStream);
                    // Read the content.  
                    string responseFromServer = await reader.ReadToEndAsync();
                    var iexSymbols = JsonConvert.DeserializeObject<List<IexSymbol>>(responseFromServer);
                    this.Create(iexSymbols).Start();
                }
            }

            // Close the response.  
            response.Close();
        }
        
        public async Task<List<IexSymbol>> Get()
        {
            var result = await _iexSymbols.FindAsync<IexSymbol>(new BsonDocument("isEnabled", true));
            return await result.ToListAsync();
        }

        public async Task<IexSymbol> Get(string id)
        {
            var result = await _iexSymbols.FindAsync<IexSymbol>(new BsonDocument("Id", id));
            return await result.FirstOrDefaultAsync();
        }

        public async Task Create(IexSymbol iexSymbol)
        {
            await _iexSymbols.InsertOneAsync(iexSymbol);
        }

        public async Task Create(IEnumerable<IexSymbol> iexSymbols)
        {
            await _iexSymbols.InsertManyAsync(iexSymbols);
        }

        public async Task Update(string id, IexSymbol iexSymbolIn)
        {
            await _iexSymbols.ReplaceOneAsync(new BsonDocument("Id", id), iexSymbolIn);
        }

        public async Task Remove(IexSymbol iexSymbolIn)
        {
            await _iexSymbols.DeleteManyAsync(new BsonDocument("iexId", iexSymbolIn.iexId));
        }

        public async Task Remove(string id)
        {
            await _iexSymbols.DeleteOneAsync(new BsonDocument("Id", id));
        }

        public async Task RemoveAll()
        {
            await _iexSymbols.DeleteManyAsync(new BsonDocument());
        }
    }
}
