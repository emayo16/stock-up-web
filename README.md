# Stock Up

Stock Up is a SPA built with Angular 9 and ASP.NET Core 3.1.

It retrieves stock data from IEX Cloud API and uses it to provide the user with analysis and visualization tools such as Intraday Price Chart for a stock symbol.

Demo Deployment Hosted on Heroku: https://stock-up-web.herokuapp.com

## Home Page

![stock-up-home.png](https://bitbucket.org/repo/7E5yL45/images/2058714045-stock-up-home.png)

Please visit the Stock Up wiki for more info: https://bitbucket.org/emayo16/stock-up-web/wiki/Home