import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { StockChartComponent } from './stock-chart/stock-chart.component';
import { StockDetailComponent } from './stock-detail/stock-detail.component';
import { StockKeyStatsComponent } from './stock-key-stats/stock-key-stats.component';
import { StockNewsArticleComponent } from './stock-news-article/stock-news-article.component';
import { StockNewsArticleListComponent } from './stock-news-article-list/stock-news-article-list.component';
import { StockNewsOverviewComponent } from './stock-news-overview/stock-news-overview.component';
import { StocksOverviewComponent } from './stocks-overview/stocks-overview.component';
import { StockQuoteListTableComponent } from './stock-quote-list-table/stock-quote-list-table.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavMenuComponent,
    StockChartComponent,
    StockDetailComponent,
    StockKeyStatsComponent,
    StockNewsArticleComponent,
    StockNewsArticleListComponent,
    StockNewsOverviewComponent,
    StocksOverviewComponent,
    StockQuoteListTableComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'stocks-overview', component: StocksOverviewComponent },
      { path: 'stock-detail/:symbol', component: StockDetailComponent },
      { path: 'news/:symbol', component: StockNewsOverviewComponent },
    ]),
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatPaginatorModule,
    MatSidenavModule,
    MatSortModule,
    MatTableModule,
    MatToolbarModule,
    MatTooltipModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
