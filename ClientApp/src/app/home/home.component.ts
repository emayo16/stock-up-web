import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';

import { IexQuote } from '../models/IexQuote';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  gainersListType: string;
  losersListType: string;
  // displayedColumns: string[] = ['symbol', 'companyName', 'high', 'low', 'open', 'close', 'change', 'changePercent'];
  // dataSource: MatTableDataSource<IexQuote>;

  // @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ViewChild(MatSort) sort: MatSort;

  constructor() {
    this.gainersListType = 'gainers';
    this.losersListType = 'losers';
  }

  ngOnInit() {
    // this.getQuoteList('gainers');
  }

  // applyFilter(filterValue: string) {
  //   this.dataSource.filter = filterValue.trim().toLowerCase();

  //   if (this.dataSource.paginator) {
  //     this.dataSource.paginator.firstPage();
  //   }
  // }

  // getQuoteList(listType: string) {
  //   this.http.get<IexQuote[]>(this.baseUrl + 'api/StockData/GetQuoteList/' + listType).subscribe(result => {
  //     // Assign the data to the data source for the table to render
  //     this.dataSource = new MatTableDataSource(result);
  //     this.dataSource.paginator = this.paginator;
  //     this.dataSource.sort = this.sort;
  //   }, error => console.error(error));
  // }
}
