import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';

import { IexKeyStats } from '../models/IexKeyStats';
import { TooltipPosition } from '@angular/material/tooltip';

@Component({
  selector: 'app-stock-detail',
  templateUrl: './stock-detail.component.html',
  styleUrls: ['./stock-detail.component.css']
})
export class StockDetailComponent implements OnInit {
  public symbol: string;
  public keyStats: IexKeyStats;
  public newsUrl: string;
  position: TooltipPosition = 'left';

  constructor(private route: ActivatedRoute, 
     private http: HttpClient, 
     @Inject('BASE_URL') private baseUrl: string) {
      this.symbol = this.route.snapshot.paramMap.get('symbol');
      this.newsUrl = this.baseUrl + 'news/' + this.symbol;
      this.getKeyStats(this.symbol);
  }

  ngOnInit() {
  }

  getKeyStats(symbol: string) {
    this.http.get<IexKeyStats>(this.baseUrl + 'api/StockData/GetKeyStats/' + symbol).subscribe(result => {
      this.keyStats = result;
    }, error => console.error(error));
  }

  onNavigate(url) {
    window.open(url, "_blank");
  }
}
