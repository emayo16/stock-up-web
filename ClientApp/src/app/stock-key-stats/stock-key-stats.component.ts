import { Component, OnInit, Input } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';

import { IexKeyStats } from '../models/IexKeyStats';

@Component({
  selector: 'app-stock-key-stats',
  templateUrl: './stock-key-stats.component.html',
  styleUrls: ['./stock-key-stats.component.css']
})
export class StockKeyStatsComponent implements OnInit {
  allColumns: string[] = ['symbol', 'companyName', 'marketcap', 'week52high', 'week52low', 'week52change', 'sharesOutstanding', 'iexfloat', 'avg10Volume', 'avg30Volume', 'day200MovingAvg', 'day50MovingAvg', 'employees', 'ttmEPS', 'ttmDividendRate', 'dividendYield', 'nextDividendDate', 'exDividendDate', 'nextEarningsDate', 'peRatio', 'beta', 'maxChangePercent', 'year5ChangePercent', 'year2ChangePercent', 'year1ChangePercent', 'ytdChangePercent', 'month6ChangePercent', 'month3ChangePercent', 'month1ChangePercent', 'day30ChangePercent', 'day5ChangePercent'];

  displayedColumns: string[] = ['symbol', 'companyName', 'marketcap', 'week52high', 'week52low', 'week52change', 'sharesOutstanding',  'avg30Volume', 'employees', 'ttmEPS', 'ttmDividendRate', 'dividendYield', 'nextDividendDate', 'exDividendDate', 'nextEarningsDate', 'peRatio', 'beta', 'maxChangePercent', 'ytdChangePercent'];
  dataSource: MatTableDataSource<IexKeyStats>;

  @Input() keyStats: IexKeyStats;

  constructor() {}

  ngOnInit() {
    this.dataSource = new MatTableDataSource([this.keyStats]);
  }
  
}
