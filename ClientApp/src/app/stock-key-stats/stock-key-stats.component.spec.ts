import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockKeyStatsComponent } from './stock-key-stats.component';

describe('StockKeyStatsComponent', () => {
  let component: StockKeyStatsComponent;
  let fixture: ComponentFixture<StockKeyStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockKeyStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockKeyStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
