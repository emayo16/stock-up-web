import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockNewsOverviewComponent } from './stock-news-overview.component';

describe('StockNewsOverviewComponent', () => {
  let component: StockNewsOverviewComponent;
  let fixture: ComponentFixture<StockNewsOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockNewsOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockNewsOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
