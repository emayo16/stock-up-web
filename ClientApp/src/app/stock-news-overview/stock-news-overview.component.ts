import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';

@Component({
  selector: 'app-stock-news-overview',
  templateUrl: './stock-news-overview.component.html',
  styleUrls: ['./stock-news-overview.component.css']
})
export class StockNewsOverviewComponent implements OnInit {
  public symbol: string;

  constructor(private route: ActivatedRoute) {
    this.symbol = this.route.snapshot.paramMap.get('symbol');
  }

  ngOnInit(): void {
  }

}
