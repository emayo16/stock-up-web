import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';

import { IexSymbol } from '../models/IexSymbol';

@Component({
  selector: 'app-stocks-overview',
  templateUrl: './stocks-overview.component.html',
  styleUrls: ['./stocks-overview.component.css']
})
export class StocksOverviewComponent implements OnInit {
  displayedColumns: string[] = ['symbol', 'exchange', 'name', 'date', 'type', 'iexId', 'region', 'currency'];
  dataSource: MatTableDataSource<IexSymbol>;
  
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {}

  ngOnInit() {
    this.getSupportedSymbols();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  getSupportedSymbols() {
    this.http.get<IexSymbol[]>(this.baseUrl + 'api/StockData/GetSupportedSymbols').subscribe(result => {
      this.dataSource = new MatTableDataSource(result);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, error => console.error(error));
  }
}
