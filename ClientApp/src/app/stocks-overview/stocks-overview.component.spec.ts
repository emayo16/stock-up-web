import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StocksOverviewComponent } from './stocks-overview.component';

describe('StocksOverviewComponent', () => {
  let component: StocksOverviewComponent;
  let fixture: ComponentFixture<StocksOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StocksOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StocksOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
