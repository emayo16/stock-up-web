export interface IexIntradayPrice {
    date: string;
    minute: string;
    label: string;
    marktOpen?: number;
    marketClose?: number;
    marktHigh?: number;
    marketLow?: number;
    marketAverage?: number;
    marketVolume?: number;
    marketNotional?: number;
    marketNumberOfTrades?: number;
    marketChangeOverTime?: number;
    high?: number;
    low?: number;
    open?: number;
    close?: number;
    average?: number;
    volume?: number;
    notional?: number;
    numberOfTrades?: number;
    changeOverTime?: number;
}
