import { Component, Inject, OnInit, ViewChild, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TooltipPosition } from '@angular/material/tooltip';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';

import { IexQuote } from '../models/IexQuote';

@Component({
  selector: 'app-stock-quote-list-table',
  templateUrl: './stock-quote-list-table.component.html',
  styleUrls: ['./stock-quote-list-table.component.css']
})
export class StockQuoteListTableComponent implements OnInit {
  @Input() listType: string;
  position: TooltipPosition = 'left';
  
  displayedColumns: string[] = ['symbol', 'companyName', 'high', 'low', 'open', 'close', 'change', 'changePercent'];
  dataSource: MatTableDataSource<IexQuote>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {}

  ngOnInit() {
    this.getQuoteList(this.listType);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getQuoteList(listType: string) {
    this.http.get<IexQuote[]>(this.baseUrl + 'api/StockData/GetQuoteList/' + listType).subscribe(result => {
      // Assign the data to the data source for the table to render
      this.dataSource = new MatTableDataSource(result);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, error => console.error(error));
  }
}
