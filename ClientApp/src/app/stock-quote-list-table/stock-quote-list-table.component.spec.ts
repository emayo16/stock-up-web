import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockQuoteListTableComponent } from './stock-quote-list-table.component';

describe('StockQuoteListTableComponent', () => {
  let component: StockQuoteListTableComponent;
  let fixture: ComponentFixture<StockQuoteListTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockQuoteListTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockQuoteListTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
