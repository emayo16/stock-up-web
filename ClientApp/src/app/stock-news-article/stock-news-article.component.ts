import { Component, OnInit, Input } from '@angular/core';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';

import { IexNewsArticle } from '../models/IexNewsArticle';
import { TooltipPosition } from '@angular/material/tooltip';

@Component({
  selector: 'app-stock-news-article',
  templateUrl: './stock-news-article.component.html',
  styleUrls: ['./stock-news-article.component.css']
})
export class StockNewsArticleComponent implements OnInit {
  @Input() newsArticle: IexNewsArticle;
  position: TooltipPosition = 'above';

  constructor() {
  }

  ngOnInit() {
  }

  onNavigate(url) {
    window.open(url, "_blank");
  }
}
