import { Component, OnInit, Input, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { from } from 'rxjs';
import { map } from 'rxjs/operators';
import * as Chart from 'chart.js';

import { IexIntradayPrice } from '../models/IexIntradayPrice';

@Component({
  selector: 'app-stock-chart',
  templateUrl: './stock-chart.component.html',
  styleUrls: ['./stock-chart.component.css']
})
export class StockChartComponent implements OnInit {
  @Input() symbol: string;
  public intradayPrices: IexIntradayPrice[];
  
  constructor(private http: HttpClient, 
    @Inject('BASE_URL') private baseUrl: string) {
  }

  ngOnInit() {
    this.getIntradayPrices(this.symbol);
  }

  getIntradayPrices(symbol: string) {
    this.http.get<IexIntradayPrice[]>(this.baseUrl + 'api/StockData/GetIntradayPrices/' + symbol).subscribe(result => {
      this.intradayPrices = result;
      this.populateCanvas();
    }, error => console.error(error));
  }

  populateCanvas () {
    let data = [], i = 0;
    from(this.intradayPrices).pipe(
      map(price => {
          return {x: price.minute, y: price.close};
      })
    ).subscribe(price => {
      //if (i++ % 5 === 0) {
        data.push(price);
      //}
    });
    
    const marketHoursLabels = ['09:30', '09:45', '10:00', '10:15', '10:30', '10:45', '11:00', '11:15', '11:30', '11:45', '12:00', '12:15', '12:30', '12:45', '13:00', '13:15', '13:30', '13:45', '14:00', '14:15', '14:30', '14:45', '15:00', '15:15', '15:30', '15:45', '16:00'];
    const myLineChart = new Chart('stockChart', {
      type: 'line',
      data: {
        labels: marketHoursLabels,
        datasets: [{
            label: 'Close',
            data: data,
            fill: false,
            pointRadius: 0,
            borderColor: 'skyblue',
            backgroundColor: 'skyblue',
            xAxisID: 'x-axis',
            yAxisID: 'y-axis'
        }]
      },
      options: {
        elements: {
          line: {
              tension: 0 // disables bezier curves
          }
        },
        responsive: true,
        title: {
          display: false,
          text: 'Intraday Prices for ' + this.symbol
        },
        tooltips: {
          mode: 'index',
          intersect: false,
        },
        hover: {
          mode: 'nearest',
          intersect: true
        },
        showLines: true,
        spanGaps: true,
        scales: {
            xAxes: [{
              display: true,
              id: 'x-axis',
              type: 'time',
              time: {
                  parser: 'HH:mm',
                  tooltipFormat: 'HH:mm',
                  unit: 'minute'
              },
              scaleLabel: {
                labelString: 'Market Time',
                display: true
              },
              ticks: {
                stepSize: 15
              }
            }],
            yAxes: [{
              display: true,
              id: 'y-axis',
              scaleLabel: {
                labelString: 'Closing Price (USD)',
                display: true
              }
            }]
        }
      } 
    });
  }
}
