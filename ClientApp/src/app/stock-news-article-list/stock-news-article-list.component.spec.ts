import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockNewsArticleListComponent } from './stock-news-article-list.component';

describe('StockNewsArticleListComponent', () => {
  let component: StockNewsArticleListComponent;
  let fixture: ComponentFixture<StockNewsArticleListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockNewsArticleListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockNewsArticleListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
