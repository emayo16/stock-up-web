import { Component, Inject, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';

import { IexNewsArticle } from '../models/IexNewsArticle';
import { TooltipPosition } from '@angular/material/tooltip';

@Component({
  selector: 'app-stock-news-article-list',
  templateUrl: './stock-news-article-list.component.html',
  styleUrls: ['./stock-news-article-list.component.css']
})
export class StockNewsArticleListComponent implements OnInit {
  @Input() symbol: string;
  @Input() count: number;
  public newsArticles: IexNewsArticle[];
  position: TooltipPosition = 'left';

  constructor(private http: HttpClient, 
     @Inject('BASE_URL') private baseUrl: string) {
  }

  ngOnInit() {
    this.getNewsArticles(this.symbol, this.count);
  }

  getNewsArticles(symbol: string, count: number) {
    this.http.get<IexNewsArticle[]>(this.baseUrl + 'api/StockData/GetNewsArticles/' + symbol + '/' + count).subscribe(result => {
      this.newsArticles = result;
    }, error => console.error(error));
  }
}
